enum CardinalDirections {
    North = 'North',
    East = 'East',
    South = 'South',
    West = 'West'
}

let CardinalDirection = CardinalDirections.East;
console.log(CardinalDirection);