interface Rectangle {
    width : number,
    height : number
}

interface ColorRectangle extends Rectangle {
    color:string
}

const rectangle:Rectangle  = {
    width:20,
    height : 10
}

const colorRectangle:ColorRectangle = {
    width : 20,
    height : 10,
    color: 'red'
}

console.log(rectangle)
console.log(colorRectangle)